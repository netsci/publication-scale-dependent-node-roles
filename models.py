import torch
import numpy as np
import torch_geometric
import networkx as nx
from torch.multiprocessing import Pool,  set_start_method
from pytorch_lightning.loggers import TensorBoardLogger
from torch.nn import  Sequential, Linear, ReLU,  Softmax, CrossEntropyLoss
from torch.optim import Adam 
from torch_geometric.data import DataLoader
from torch_geometric.nn import global_add_pool, GINConv
from pytorch_lightning import LightningModule, Trainer
from pytorch_lightning.metrics import Metric, Precision
from pytorch_lightning.metrics.utils import to_categorical
from ray.tune.integration.pytorch_lightning import TuneReportCallback
import torch.nn.functional as F
import ray.tune as tune
import sys


# Adaptation of lightnings Accuracy for categorical input
class MyAccuracy(Metric):
    def __init__(self, dist_sync_on_step=False):
        super().__init__(dist_sync_on_step=dist_sync_on_step)

        self.add_state("correct", default=torch.tensor(0), dist_reduce_fx="sum")
        self.add_state("total", default=torch.tensor(0), dist_reduce_fx="sum")

    def update(self, preds: torch.Tensor, target: torch.Tensor):
        assert preds.shape[0] == target.shape[0]

        self.correct += torch.sum(to_categorical(preds, argmax_dim=1) == target)
        self.total += target.shape[0]

    def compute(self):
        return self.correct.float() / self.total




# GIN Module 
class GIN_Module(LightningModule):
    def __init__(self, config, out_size, embedding_size: int = 32, initialization=torch.nn.init.ones_, out_classifier=None, gin_nn=None):
        super().__init__()

        # hyperparameters
        self.embedding_size = embedding_size
        self.num_layers = config['num_layers']
        self.initialization = initialization
        self.learning_rate = config['learning_rate']
        self.regularization = config['regularization']
        self.loss_func = CrossEntropyLoss()
        self.activation_func = Softmax(dim=1)
        self.relu = ReLU()

        # sub-modules
        # Graph Convolution Module for the GIN.
        gin_nn = gin_nn if gin_nn != None else [Sequential(
            Linear(self.embedding_size, 2 * self.embedding_size),
            ReLU(),
            Linear(2 * self.embedding_size, self.embedding_size)
        ) for i in range(self.num_layers)]
        # Convolutional layer that is different for each layer.
        self.convs = torch.nn.ModuleList([GINConv(gin_nn[i]) for i in range(self.num_layers)])
        # Output MLP
        self.out = out_classifier if out_classifier != None else Sequential(
            Linear(self.embedding_size, 2 * self.embedding_size),
            ReLU(),
            Linear(2 * self.embedding_size, 2 * self.embedding_size),
            ReLU(),
            Linear(2 * self.embedding_size, out_size)
        )

        # metrics
        self.train_acc = MyAccuracy()
        self.val_prec = Precision(num_classes=out_size)
        self.val_acc = MyAccuracy()

        self.reset_parameters()

    def forward(self, data):
        embeddings = torch.empty((data.num_nodes, self.embedding_size), device=self.device)
        # Standard initialization is constant
        self.initialization(embeddings)
        # If the GNN is informed, the data is written into the initialisation. (If uninformed, data.x is the all-ones-vector.)
        embeddings[:, :data.x.shape[1]] = data.x
        embeddings.requires_grad = True
        
        # Compute the Graph Convolutions
        for it in range(self.num_layers):
            embeddings = self.relu(self.convs[it](embeddings, data.edge_index))

        # return the output of the MLP
        return self.out(embeddings)
        

    def training_step(self, batch, batch_idx):
        # Forward pass
        prediction = self(batch)

        # Compute loss
        loss = self.loss_func(prediction, batch.y)

        # Logging
        self.train_acc(self.activation_func(prediction), batch.y)
        self.log('train_acc', self.train_acc, on_epoch=True, prog_bar=True)

        # Loss is passed to backward pass
        return loss

    def validation_step(self, batch, batch_idx):
        # Forward pass
        prediction = self(batch)
        loss = self.loss_func(prediction, batch.y)

        # Logging
        self.val_acc(self.activation_func(prediction), batch.y)
        self.val_prec(self.activation_func(prediction), batch.y)
        self.log('val_loss', loss)
        self.log('val_prec', self.val_prec, on_epoch=True, prog_bar=True)
        self.log('val_acc', self.val_acc, on_epoch=True, prog_bar=True)

    def reset_parameters(self):
        for comp in self.convs:
            comp.reset_parameters()
        torch.nn.init.xavier_normal_(self.out[0].weight)
        torch.nn.init.xavier_normal_(self.out[2].weight)
        torch.nn.init.xavier_normal_(self.out[4].weight)

    def configure_optimizers(self):
        return Adam(self.parameters(), lr=self.learning_rate, weight_decay=self.regularization)

    def __module_name__(self):
        return 'GIN_Module'



# SNP - Embedding plus MLP output layer. 
class sortedNPNet_Module(LightningModule):
    def __init__(self, config, out_size, embedding_size: int = 32, num_layers: int = 3,
                out_classifier=None):
        super().__init__()

        # hyperparameters
        self.embedding_size = embedding_size
        self.num_layers = num_layers
        self.learning_rate = config['learning_rate']
        self.loss_func = CrossEntropyLoss()
        self.activation_func = Softmax(dim=1)
        self.relu = ReLU()
        self.regularization = config['regularization']

        # Output MLP
        self.out = out_classifier if out_classifier != None else Sequential(
            Linear((self.num_layers+1)*self.embedding_size, 2 * self.embedding_size),
            ReLU(),
            Linear(2 * self.embedding_size, 2 * self.embedding_size),
            ReLU(),
            Linear(2 * self.embedding_size, out_size)
        )

        # metrics
        self.train_acc = MyAccuracy()
        self.val_prec = Precision(num_classes=out_size)
        self.val_acc = MyAccuracy()

        self.left_outputs = []
        self.right_outputs = []
        self.reset_parameters()

    # The SNP embeddings are pre-computed. 
    def forward(self, data):
        return self.out(data.snp_embeddings)
        

    def training_step(self, batch, batch_idx):
        # Forward pass
        prediction = self(batch)

        # Compute Loss
        loss = self.loss_func(prediction, batch.y)

        # Logging
        self.train_acc(self.activation_func(prediction), batch.y)
        self.log('train_acc', self.train_acc, on_epoch=True, prog_bar=True)
        self.log('train_loss', loss)
        return loss

    def validation_step(self, batch, batch_idx):
        # Forward pass
        prediction = self(batch)
        loss = self.loss_func(prediction, batch.y)

        # Logging
        self.val_acc(self.activation_func(prediction), batch.y)
        self.val_prec(self.activation_func(prediction), batch.y)
        self.log('val_loss', loss)
        self.log('val_prec', self.val_prec, on_epoch=True, prog_bar=True)
        self.log('val_acc', self.val_acc, on_epoch=True, prog_bar=True)

    def reset_parameters(self):
        pass

    def configure_optimizers(self):
        return Adam(self.parameters(), lr=self.learning_rate, weight_decay=self.regularization)

    def __module_name__(self):
        return 'sortedNPNet_Module'



# Global Module that is used for the graph-level task. It is used on top of the node-level module that is accepts as a parameter.
class Global_Module(LightningModule):
    def __init__(self, config, module, out_size, embedding_size: int = 32, gl_out_classifier=None):
        super().__init__()

        # hyperparameters
        self.embedding_size = embedding_size
        self.num_layers = config['num_layers']
        self.learning_rate = config['learning_rate']
        self.regularization = config['regularization']
        self.loss_func = CrossEntropyLoss()
        self.activation_func = Softmax(dim=1)
        self.relu = ReLU()

        # sub-modules
        # node-level GNN
        self.module = module

        # Global output MLP
        self.gl_out = gl_out_classifier if gl_out_classifier != None else Sequential(
            Linear(self.embedding_size, 2 * self.embedding_size),
            ReLU(),
            Linear(2 * self.embedding_size, 2 * self.embedding_size),
            ReLU(),
            Linear(2 * self.embedding_size, out_size)
        )

        # metrics
        self.train_acc = MyAccuracy()
        self.val_prec = Precision(num_classes=out_size)
        self.val_acc = MyAccuracy()

        self.reset_parameters()

    def forward(self, data):
        # node-level GNN forward pass
        embeddings = self.module(data)
        # Global pooling of node embeddings
        embeddings = global_add_pool(embeddings, data.batch)
        # Return gobal out MLP
        return self.gl_out(embeddings)

    def training_step(self, batch, batch_idx):
        # Forward pass
        prediction = self(batch)
        
        # Compute loss
        loss = self.loss_func(prediction, batch.y)

        # Logging
        self.train_acc(self.activation_func(prediction), batch.y)
        self.log('train_acc', self.train_acc, on_epoch=True, prog_bar=True)

        return loss

    def validation_step(self, batch, batch_idx):
        # Forward pass
        prediction = self(batch)
        loss = self.loss_func(prediction, batch.y)

        # Logging
        self.val_acc(self.activation_func(prediction), batch.y)
        self.val_prec(self.activation_func(prediction), batch.y)
        self.log('val_loss', loss)
        self.log('val_prec', self.val_prec, on_epoch=True, prog_bar=True)
        self.log('val_acc', self.val_acc, on_epoch=True, prog_bar=True)

    def reset_parameters(self):
        self.module.reset_parameters()

    def configure_optimizers(self):
        return Adam(self.parameters(), lr=self.learning_rate, weight_decay=self.regularization)

    def __module_name__(self):
        return 'Global_'+self.module.__module_name__()
